# Experiments with Pony-lang and Nix

I started setting up the env with Nix via the manual and [some posts](https://ariya.io/2016/05/nix-as-os-x-package-manager)
And particularly the [isolated dev environmnet with nix](https://ariya.io/2016/06/isolated-development-environment-using-nix)

## Running this sample

1. Install Nix: `bash <(curl https://nixos.org/nix/install)`
2. Start a Nix shell: `nix-shell`
3. Compile: `ponyc src/main/hello-pony/ -o target/bin/`
4. Run: `target/bin/hello-pony`
