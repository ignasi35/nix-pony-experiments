use "net/http"
use "time"

actor Main
  """
  A simple HTTP server based on https://github.com/ponylang/ponyc/blob/0.15.0/examples/httpserver/httpserver.pony
  """
  new create(env:Env) =>
    let service = "9090"  // IDK what service is
    let limit:USize = 100 // IDK what limit is
    let host = "localhost"

    // a Pony HTTPServer must have few things: https://stdlib.ponylang.org/net-http-HTTPServer
    // 1. a logger
    // 2. an auth (IDK what that is)
    // 3. a ServerNotify and a HandlerFactory
    let logger = CommonLog(env.out)
    // let logger = ContentsLog(env.out)
    // let logger = DiscardLog

    // I need to read about Auth in general, IDK what's going on here.
    let auth = try
      env.root as AmbientAuth
    else
      env.out.print("unable to use network")
      return
    end

    HTTPServer(
      auth,
      ListenHandler(env), // (see below) this is a `ServerNotify` instance
      BackendMaker.create(env), // (see below) this is a `HandlerFactory` instance
      logger // hmmm, there's no [,] (comma) here, wo `where` is part of logger ?
      where service=service, host=host, limit=limit, reversedns=auth)

// this class is used as a ServerNotify above but it doesn't
// implement the interface. Structural typing?
class ListenHandler //is ServerNotify
  let _env: Env

  new iso create(env: Env) =>
    _env = env

  fun ref listening(server: HTTPServer ref) =>
    try
      (let host, let service) = server.local_address().name()
    else
      _env.out.print("Can't get local address.")
      server.dispose()
    end

  fun ref not_listening(server: HTTPServer ref) =>
    _env.out.print("Failed to listen.")

  fun ref closed(server: HTTPServer ref) =>
    _env.out.print("Shutdown.")

class BackendMaker is HandlerFactory
  let _env: Env

  new val create(env: Env) =>
    _env = env

  // IDK what the [^] means. Subclass of ?
  fun apply(session: HTTPSession): HTTPHandler^ =>
    BackendHandler.create(_env, session)

class BackendHandler is HTTPHandler
  let _env: Env
  let _session: HTTPSession

  new ref create(env: Env, session: HTTPSession) =>
    _env = env
    _session = session

  fun ref apply(request: Payload val) =>
    let response = Payload.response()
    response.add_chunk("You asked for ")
    response.add_chunk(request.url.path)

    response.add_chunk(" at ")
    let s = Time.seconds()
    response.add_chunk(s.string())

    if request.url.query.size() > 0 then
      response.add_chunk("?")
      response.add_chunk(request.url.query)
    end

    _session(consume response)
